from datetime import datetime
import time
import os
import configparser
import ProcessKill

config = configparser.ConfigParser()


def getdate(date_str, date_format='%Y-%m-%dT%H:%M:%S'):
    return datetime.strptime(date_str, date_format)


def check_schedule():
    time.sleep(60)
    while True:
        config.read_file(open('config.ini'))
        scheduled_time = config.get('schedule', 'time')
        process = config.get('process', 'name')
        date_obj = getdate(scheduled_time)
        if (date_obj - datetime.now()).total_seconds() <= 0.0 and datetime.now().date() == date_obj.date():
            ProcessKill.kill_process(process)
            shutdown()
        time.sleep(60)


def killall():
    os.system('sudo pkill -f python')


def shutdown():
    time.sleep(5)
    os.system('sudo shutdown -h now')


if __name__ == '__main__':
    check_schedule()
