import os

default_process = "main.py"


def kill_process(process=default_process):
    pid = None
    processes = os.popen("ps -ef | grep " + process).read()
    processes = processes.split("\n")
    processes = processes[0].split(" ")
    # print(processes);
    for p in processes:
        try:
            pid = int(p)
            break
        except ValueError:
            continue
    if pid:
        os.system("kill " + str(pid))


if __name__ == '__main__':
    kill_process()
