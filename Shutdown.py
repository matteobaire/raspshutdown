import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def shutdown():
    time.sleep(5)
    os.system("pkill -f python")
    os.system("sudo shutdown -h now")


GPIO.add_event_detect(40, GPIO.FALLING,callback=shutdown, bouncetime=2000)


if __name__ == '__main__':
    while True:
        time.sleep(1)
